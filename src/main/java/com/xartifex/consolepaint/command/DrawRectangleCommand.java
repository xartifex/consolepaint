package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.core.Point;

public class DrawRectangleCommand extends Command {
    private final Point upperLeft;
    private final Point lowerRight;

    DrawRectangleCommand(CommandEnum command, Integer[] args) {
        super(command, args);
        upperLeft = new Point(args[0], args[1]);
        lowerRight = new Point(args[2], args[3]);
    }

    public Point getUpperLeft() {
        return upperLeft;
    }

    public Point getLowerRight() {
        return lowerRight;
    }

    @Override
    public String toString() {
        return "DrawRectangleCommand{" +
                "upperLeft=" + upperLeft +
                ", lowerRight=" + lowerRight +
                "} " + super.toString();
    }
}
