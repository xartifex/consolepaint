package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.exception.EmptyCommandException;
import com.xartifex.consolepaint.exception.InvalidArgumentException;
import com.xartifex.consolepaint.exception.NotEnoughParametersException;
import com.xartifex.consolepaint.exception.UnknownCommandException;

import java.util.*;

public class CommandResolver {
    public Command resolve(String input) {
        input = input.replaceAll("\\s{2,}", " ").trim();
        Scanner scanner = new Scanner(input);
        scanner.useDelimiter(" ");
        if (!scanner.hasNext()) {
            throw new EmptyCommandException();
        }
        String commandName = scanner.next();
        CommandEnum commandEnum = null;
        try {
            commandEnum = CommandEnum.valueOf(commandName.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            throw new UnknownCommandException("Unknown commandEnum: " + commandName);
        }
        try {
            switch (commandEnum) {
                case C: {
                    int w = Integer.valueOf(scanner.next());
                    int h = Integer.valueOf(scanner.next());
                    Integer[] args = {w, h};
                    return new CreateCommand(commandEnum, args);
                }
                case L: {
                    int x1 = Integer.valueOf(scanner.next());
                    int y1 = Integer.valueOf(scanner.next());
                    int x2 = Integer.valueOf(scanner.next());
                    int y2 = Integer.valueOf(scanner.next());
                    Integer[] args = {x1, y1, x2, y2};
                    return new DrawLineCommand(commandEnum, args);
                }
                case R: {
                    int x1 = Integer.valueOf(scanner.next());
                    int y1 = Integer.valueOf(scanner.next());
                    int x2 = Integer.valueOf(scanner.next());
                    int y2 = Integer.valueOf(scanner.next());
                    Integer[] args = {x1, y1, x2, y2};
                    return new DrawRectangleCommand(commandEnum, args);
                }
                case B: {
                    int x = Integer.valueOf(scanner.next());
                    int y = Integer.valueOf(scanner.next());
                    String str = " ";
                    if (scanner.hasNext()) {
                        str = scanner.next();
                    }
                    char c = str.charAt(0);
                    Object[] args = {x, y, c};
                    return new FillCommand(commandEnum, args);
                }
                case Q: {
                    return QuitCommand.QUIT;
                }
                case H: {
                    return HelpCommand.HELP;
                }
            }
        } catch (NumberFormatException e) {
            throw new InvalidArgumentException(commandEnum, e);
        } catch (IllegalArgumentException e) {
            throw new InvalidArgumentException(commandEnum, e);
        } catch (NoSuchElementException e) {
            throw new NotEnoughParametersException(commandEnum, e);
        }
        //should never reach here
        throw new IllegalStateException("Invalid Program State");
    }
}
