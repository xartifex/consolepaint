package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.core.Canvas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;

import static com.xartifex.consolepaint.CommonConstants.NL;

public class CommandExecutor {
    private static final Logger LOGGER = LoggerFactory.getLogger(CommandExecutor.class);

    private final PrintWriter printWriter;

    public CommandExecutor(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public Canvas executeCreate(CreateCommand command) {
        try {
            Canvas canvas = new Canvas(command.getWidth(), command.getHeight());
            canvas.draw(printWriter);
            return canvas;
        } catch (IllegalArgumentException e) {
            LOGGER.error("Unable to create a canvas: ", e);
            printWriter.println(e.getMessage());
            return null;
        } finally {
            printWriter.flush();
        }
    }

    public void executeDrawLine(DrawLineCommand command, Canvas canvas) {
        execute(() -> canvas.drawLine(command.getStart(), command.getEnd()), canvas);
    }

    public void executeDrawRectangle(DrawRectangleCommand command, Canvas canvas) {
        execute(() -> canvas.drawRectangle(command.getUpperLeft(), command.getLowerRight()), canvas);
    }

    public void executeFill(FillCommand command, Canvas canvas) {
        execute(() -> canvas.fill(command.getStart(), command.getColor()), canvas);
    }

    public void executeHelp() {
        printWriter.write("Command          Description" + NL);
        for (CommandEnum commandEnum : CommandEnum.values()) {
            StringBuilder helpSection = new StringBuilder();
            String nameParameters = commandEnum.getName() + " "
                    + commandEnum.getParameters();
            helpSection.append(nameParameters);
            for(int i = 0; i < 17 - nameParameters.length(); i++) {
                helpSection.append(' ');
            }
            printWriter.print(helpSection.append(commandEnum.getDescription()).append(NL)
                    .toString());
        }
        printWriter.flush();
    }

    public void executeQuit() {
        printWriter.println("Bye!");
        printWriter.flush();
    }

    private void execute(Action action, Canvas canvas) {
        try {
            action.execute();
            canvas.draw(printWriter);
        } catch (IllegalArgumentException e) {
            LOGGER.error("Unable to execute action: ", e);
            printWriter.println(e.getMessage() + ". Enter H for help.");
        } finally {
            printWriter.flush();
        }
    }
}
