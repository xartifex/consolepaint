package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.CommonConstants;

public class HelpCommand extends Command {
    public static final HelpCommand HELP = new HelpCommand(CommandEnum.H);

    private HelpCommand(CommandEnum command) {
        super(command, CommonConstants.EMPTY_OBJ_ARRAY);
    }
}
