package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.core.Point;

public class FillCommand extends Command {
    private final Point start;
    private final char color;

    FillCommand(CommandEnum command, Object[] args) {
        super(command, args);
        start = new Point((int)args[0], (int)args[1]);
        color = (char)args[2];

    }

    public Point getStart() {
        return start;
    }

    public char getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "FillCommand{" +
                "start=" + start +
                ", color=" + color +
                "} " + super.toString();
    }
}
