package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.CommonConstants;

public class QuitCommand extends Command {
    public static final QuitCommand QUIT = new QuitCommand(CommandEnum.Q);

    private QuitCommand(CommandEnum command) {
        super(command, CommonConstants.EMPTY_OBJ_ARRAY);
    }
}
