package com.xartifex.consolepaint.command;


import java.util.Arrays;

public class Command {
    private final CommandEnum command;
    private final Object[] args;

    Command(CommandEnum command, Integer[] args) {
        this.command = command;
        this.args = args;
    }

    Command(CommandEnum command, Object[] args) {
        this.command = command;
        this.args = args;
    }

    public CommandEnum getCommand() {
        return command;
    }

    public Object[] getArgs() {
        return args;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Command)) return false;

        Command command1 = (Command) o;

        if (command != command1.command) return false;
        return Arrays.equals(args, command1.args);
    }

    @Override
    public int hashCode() {
        int result = command.hashCode();
        result = 31 * result + Arrays.hashCode(args);
        return result;
    }

    @Override
    public String toString() {
        return "Command{" +
                "command=" + command +
                ", args=" + Arrays.toString(args) +
                '}';
    }
}
