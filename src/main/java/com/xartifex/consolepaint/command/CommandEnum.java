package com.xartifex.consolepaint.command;

import static com.xartifex.consolepaint.CommonConstants.MAX_HEIGHT;
import static com.xartifex.consolepaint.CommonConstants.MAX_WIDTH;
import static com.xartifex.consolepaint.CommonConstants.NL;

public enum CommandEnum {
    C("C", "w h", "Creates a new canvas of width w and height h. " + NL +
            "                 Max width is " + MAX_WIDTH + ". Max height is " + MAX_HEIGHT + ".")
    ,
    L("L","x1 y1 x2 y2 ", "Creates a new line from (x1,y1) to (x2,y2). Currently only" + NL +
            "                 horizontal or vertical lines are supported. Horizontal and vertical lines" + NL +
            "                 will be drawn using the 'x' character."),
    R("R", "x1 y1 x2 y2", "Creates a new rectangle, whose upper left corner is (x1,y1) and" + NL +
            "                 lower right corner is (x2,y2). Horizontal and vertical lines will be drawn" + NL +
            "                 using the 'x' character."),
    B("B", "x y c", "Fills the entire area connected to (x,y) with \"colour\" c. The" + NL +
            "                 behaviour of this is the same as that of the \"bucket fill\" tool in paint" + NL +
            "                 programs."),
    Q("Q", "", "Quits the program."),
    H("H", "", "Prints information on available commands.");

    private final String name;
    private final String parameters;
    private final String description;

    CommandEnum(String name, String parameters, String description) {
        this.name = name;
        this.parameters = parameters;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getParameters() {
        return parameters;
    }

    public String getDescription() {
        return description;
    }
}
