package com.xartifex.consolepaint.command;

public class CreateCommand extends Command {
    private final int width;
    private final int height;

    CreateCommand(CommandEnum command, Integer[] args) {
        super(command, args);
        width = args[0];
        height = args[1];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CreateCommand that = (CreateCommand) o;

        if (width != that.width) return false;
        return height == that.height;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + width;
        result = 31 * result + height;
        return result;
    }

    @Override
    public String toString() {
        return "CreateCommand{" +
                "width=" + width +
                ", height=" + height +
                "} " + super.toString();
    }
}
