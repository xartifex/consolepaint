package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.core.Point;

public class DrawLineCommand extends Command {

    private final Point start;
    private final Point end;

    DrawLineCommand(CommandEnum command, Integer[] args) {
        super(command, args);
        start = new Point(args[0], args[1]);
        end = new Point(args[2], args[3]);
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "DrawLineCommand{" +
                "start=" + start +
                ", end=" + end +
                "} " + super.toString();
    }
}
