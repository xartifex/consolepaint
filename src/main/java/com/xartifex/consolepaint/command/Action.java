package com.xartifex.consolepaint.command;

public interface Action {
    void execute();
}
