package com.xartifex.consolepaint.core;

import com.xartifex.consolepaint.CommonConstants;

public class Point {
    private final int x;
    private final int y;
    /*
        ------------>x
        |
        |
        |
        |
        >y

     */
    public Point(int x, int y) {
        if(x > CommonConstants.MAX_X) {
            throw new IllegalArgumentException("Coordinate x is too big: " + x);
        }
        if(x < CommonConstants.MIN_X) {
            throw new IllegalArgumentException("Coordinate x is too small: " + x);
        }
        if(y > CommonConstants.MAX_Y) {
            throw new IllegalArgumentException("Coordinate y is too big: " + y);
        }
        if(y < CommonConstants.MIN_Y) {
            throw new IllegalArgumentException("Coordinate y is too small: " + y);
        }
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        return y == point.y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "{" + x + "," + y +"}";
    }
}
