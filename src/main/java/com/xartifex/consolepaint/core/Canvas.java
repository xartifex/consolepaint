package com.xartifex.consolepaint.core;

import com.xartifex.consolepaint.exception.ConsolePaintException;

import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import static com.xartifex.consolepaint.CommonConstants.MAX_HEIGHT;
import static com.xartifex.consolepaint.CommonConstants.MAX_WIDTH;
import static com.xartifex.consolepaint.CommonConstants.NL;

public class Canvas {
    public static final char H_BORDER = '-';
    public static final char V_BORDER = '|';
    public static final char LINE_POINT = 'x';
    public static final char EMPTY_SPACE = ' ';

    private final int width;
    private final int height;
    private final Map<Point, Character> pointToCharacter;

    public Canvas(int width, int height) {
        if (width < 1 || width > MAX_WIDTH) {
            throw new IllegalArgumentException("Invalid width: " + width + ". Valid range between 1 and " + MAX_WIDTH);
        }
        if (height < 1 || height > MAX_HEIGHT) {
            throw new IllegalArgumentException("Invalid height: " + height + ". Valid range between 1 and " + MAX_HEIGHT);
        }
        this.width = width;
        this.height = height;
        pointToCharacter = new HashMap<Point, Character>(width * height);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void draw(Writer printWriter) {
        try {
            int width = this.width + 1;
            int height = this.height + 1;
            for (int j = 0; j <= height; j++) {
                for (int i = 0; i <= width; i++) {
                    if (j == 0 || j == height) {
                        printWriter.write(H_BORDER);
                        if (i == width) {
                            printWriter.write(NL);
                        }
                        continue;
                    }
                    if (i == 0 || i == width) {
                        printWriter.write(V_BORDER);
                        if (i == width) {
                            printWriter.write(NL);
                        }
                        continue;
                    }

                    Character c = pointToCharacter.get(new Point(i, j));
                    if (c != null) {
                        printWriter.write(c);
                    } else {
                        printWriter.write(EMPTY_SPACE);
                    }
                }
            }
        } catch (Exception e) {
            throw new ConsolePaintException("Unable to draw a canvas", e);
        }
    }

    private Point adjustPoint(Point point) {
        int x = point.getX();
        int y = point.getY();
        if (x < 1) {
            x = 1;
        }
        if (x > width) {
            x = width;
        }
        if (y < 1) {
            y = 1;
        }
        if (y > height) {
            y = height;
        }
        return new Point(x, y);
    }

    public void drawLine(Point start, Point end) {
        if (start == null) {
            throw new IllegalArgumentException("start cannot be null!");
        }

        if (end == null) {
            throw new IllegalArgumentException("end cannot be null!");
        }

        if (!isHorizontalOrVertical(start, end)) {
            throw new IllegalArgumentException("Only horizontal or vertical " +
                    "lines are supported: start: " + start + " end: " + end);
        }

        if (isSwapNeeded(start, end)) {
            Point temp = start;
            start = end;
            end = temp;
        }

        if (isLineNotInRange(start, end)) {
            throw new IllegalArgumentException("The line provided " +
                    "is out of canvas range: start: " + start + " end: " + end + ". " + this);
        }

        start = adjustPoint(start);
        end = adjustPoint(end);

        int x1 = start.getX();
        int y1 = start.getY();
        int x2 = end.getX();
        int y2 = end.getY();

        //horizontal line
        if (y1 == y2) {
            for (int x = x1; x <= x2; x++) {
                pointToCharacter.put(new Point(x, y1), LINE_POINT);
            }
        }

        //vertical line
        if (x1 == x2) {
            for (int y = y1; y <= y2; y++) {
                pointToCharacter.put(new Point(x1, y), LINE_POINT);
            }
        }
    }

    public void drawRectangle(Point upperLeft, Point lowerRight) {
        if (upperLeft == null) {
            throw new IllegalArgumentException("upperLeft cannot be null!");
        }
        if (lowerRight == null) {
            throw new IllegalArgumentException("lowerRight cannot be null!");
        }

        int x1 = upperLeft.getX();
        int y1 = upperLeft.getY();
        int x2 = lowerRight.getX();
        int y2 = lowerRight.getY();

        if ((x1 > x2) || (y1 > y2)) {
            throw new IllegalArgumentException("upperLeft point is not actually upper " +
                    "and on the left from the lowerRight point");
        }

        if (isLineNotInRange(new Point(x1, y1), new Point(x1, y2)) &&
                isLineNotInRange(new Point(x2, y1), new Point(x2, y2)) &&
                isLineNotInRange(new Point(x1, y1), new Point(x2, y1)) &&
                isLineNotInRange(new Point(x1, y2), new Point(x2, y2))) {
            throw new IllegalArgumentException("Rectangle is out of canvas: upperLeft: "
                    + upperLeft + " lowerRight" + lowerRight + ". " + this);
        }

        for (int x = x1; x <= x2; x++) {
            pointToCharacter.put(new Point(x, y1), LINE_POINT);
            pointToCharacter.put(new Point(x, y2), LINE_POINT);
        }

        for (int y = y1; y <= y2; y++) {
            pointToCharacter.put(new Point(x1, y), LINE_POINT);
            pointToCharacter.put(new Point(x2, y), LINE_POINT);
        }
    }

    public void fill(Point start, char color) {
        if (start == null) {
            throw new IllegalArgumentException("start cannot be null!");
        }

        if (!isWithinRange(start)) {
            throw new IllegalArgumentException("start is out of range: " + start + " " + this);
        }

        Queue<Point> queue = new LinkedList<>();
        Point point = start;
        Character originalColor = pointToCharacter.get(point);
        if (!isColoringNeeded(point, originalColor, color)) {
            return;
        }
        pointToCharacter.put(point, color);
        while (point != null) {
            Point curUp = new Point(point.getX(), point.getY() - 1);
            Point curDown = new Point(point.getX(), point.getY() + 1);
            if (isColoringNeeded(curUp, originalColor, color)) {
                pointToCharacter.put(curUp, color);
                queue.offer(curUp);
            }

            if (isColoringNeeded(curDown, originalColor, color)) {
                pointToCharacter.put(curDown, color);
                queue.offer(curDown);
            }

            Point left = new Point(point.getX() - 1, point.getY());
            while (isColoringNeeded(left, originalColor, color)) {
                pointToCharacter.put(left, color);
                Point up = new Point(left.getX(), left.getY() - 1);
                if (isColoringNeeded(up, originalColor, color)) {
                    pointToCharacter.put(up, color);
                    queue.offer(up);
                }
                Point down = new Point(left.getX(), left.getY() + 1);
                if (isColoringNeeded(down, originalColor, color)) {
                    pointToCharacter.put(down, color);
                    queue.offer(down);
                }
                left = new Point(left.getX() - 1, left.getY());
            }

            Point right = new Point(point.getX() + 1, point.getY());
            while (isColoringNeeded(right, originalColor, color)) {
                pointToCharacter.put(right, color);
                Point up = new Point(right.getX(), right.getY() - 1);
                if (isColoringNeeded(up, originalColor, color)) {
                    pointToCharacter.put(up, color);
                    queue.offer(up);
                }
                Point down = new Point(right.getX(), right.getY() + 1);
                if (isColoringNeeded(down, originalColor, color)) {
                    pointToCharacter.put(down, color);
                    queue.offer(down);
                }
                right = new Point(right.getX() + 1, right.getY());
            }
            point = queue.poll();
        }
    }

    private boolean isWithinRange(Point point) {
        int x = point.getX();
        int y = point.getY();
        return x >= 1 && x <= width && y >= 1 && y <= height;
    }

    private boolean isColoringNeeded(Point point, Character originalColor, Character newColor) {
        Character pointValue = pointToCharacter.get(point);
        return (isWithinRange(point) && !newColor.equals(pointValue) && ((originalColor == null && pointValue == null)
                || (originalColor != null && originalColor.equals(pointValue))));
    }

    private boolean isHorizontalOrVertical(Point start, Point end) {
        int x1 = start.getX();
        int y1 = start.getY();
        int x2 = end.getX();
        int y2 = end.getY();

        return !(x1 != x2 && y1 != y2);
    }

    //this method assumes that a given line is horizontal or vertical
    private boolean isLineNotInRange(Point start, Point end) {
        int x1 = start.getX();
        int y1 = start.getY();
        int x2 = end.getX();
        int y2 = end.getY();
        if (y1 == y2 && ((y1 < 1 || y1 > height) || (x1 < 1 && x2 < 1) || (x1 > width && x2 > width))) {
            return true;
        } else if (x1 == x2 && ((x1 < 1 || x1 > width) || (y1 < 1 && y2 < 1) || (y1 > height && y2 > height))) {
            return true;
        }
        return false;
    }

    private boolean isSwapNeeded(Point left, Point right) {
        int x1 = left.getX();
        int y1 = left.getY();
        int x2 = right.getX();
        int y2 = right.getY();
        if (y1 == y2) {
            return x1 > x2;
        } else if (x1 == x2) {
            return y1 > y2;
        }
        throw new IllegalStateException("Only horizontal or vertical " +
                "lines should have been passed:  left: " + left + " right: " + right);
    }

    @Override
    public String toString() {
        return "Canvas{" +
                "width=" + width +
                ", height=" + height +
                '}';
    }
}
