package com.xartifex.consolepaint.exception;


public class UnknownCommandException extends ConsolePaintException {
    public UnknownCommandException(String message) {
        super(message);
    }
}
