package com.xartifex.consolepaint.exception;

public class ConsolePaintException extends RuntimeException {
    public ConsolePaintException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConsolePaintException(String message) {
        super(message);
    }
}
