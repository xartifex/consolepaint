package com.xartifex.consolepaint.exception;


import com.xartifex.consolepaint.command.CommandEnum;

public class InvalidArgumentException extends ConsolePaintException {
    private final CommandEnum commandEnum;

    public InvalidArgumentException(CommandEnum commandEnum, Throwable e) {
        super(e.getMessage(), e);
        this.commandEnum = commandEnum;
    }

    public CommandEnum getCommandEnum() {
        return commandEnum;
    }
}
