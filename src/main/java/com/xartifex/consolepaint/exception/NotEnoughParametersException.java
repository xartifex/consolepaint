package com.xartifex.consolepaint.exception;

import com.xartifex.consolepaint.command.CommandEnum;

public class NotEnoughParametersException extends ConsolePaintException {
    private final CommandEnum commandEnum;

    public NotEnoughParametersException(CommandEnum commandEnum, Throwable e) {
        super("Not enough parameters passed to a command!", e);
        this.commandEnum = commandEnum;
    }

    public NotEnoughParametersException(CommandEnum commandEnum) {
        super("Not enough parameters passed to a command!");
        this.commandEnum = commandEnum;
    }

    public CommandEnum getCommandEnum() {
        return commandEnum;
    }
}
