package com.xartifex.consolepaint.exception;

public class EmptyCommandException extends ConsolePaintException {

    public EmptyCommandException() {
        super("Empty command!");
    }
}
