package com.xartifex.consolepaint;

import com.xartifex.consolepaint.command.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import com.xartifex.consolepaint.core.Canvas;
import com.xartifex.consolepaint.exception.EmptyCommandException;
import com.xartifex.consolepaint.exception.InvalidArgumentException;
import com.xartifex.consolepaint.exception.NotEnoughParametersException;
import com.xartifex.consolepaint.exception.UnknownCommandException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsolePaint {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConsolePaint.class);

    public static void main(String[] args) {
        try {
            CommandResolver commandResolver = new CommandResolver();
            CommandExecutor commandExecutor = new CommandExecutor(new PrintWriter(System.out));
            try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
                Command userCommand = null;
                Canvas canvas = new Canvas(20, 4);
                while (!QuitCommand.QUIT.equals(userCommand)) {
                    try {
                        System.out.print("enter command: ");
                        String userInput = br.readLine();
                        if (userInput == null) {
                            userInput = "";
                        }
                        userCommand = commandResolver.resolve(userInput);
                        CommandEnum commandEnum = userCommand.getCommand();
                        switch (commandEnum) {
                            case C: {
                                canvas = commandExecutor.executeCreate((CreateCommand) userCommand);
                                break;
                            }
                            case L: {
                                commandExecutor.executeDrawLine((DrawLineCommand) userCommand, canvas);
                                break;
                            }
                            case R: {
                                commandExecutor
                                        .executeDrawRectangle((DrawRectangleCommand) userCommand, canvas);
                                break;
                            }
                            case B: {
                                commandExecutor.executeFill((FillCommand) userCommand, canvas);
                                break;
                            }
                            case Q: {
                                commandExecutor.executeQuit();
                                break;
                            }
                            case H: {
                                commandExecutor.executeHelp();
                                break;
                            }
                            default:
                                throw new IllegalStateException("Unexpected program state!");
                        }
                    } catch (UnknownCommandException e) {
                        LOGGER.error(e.getMessage(), e);
                        System.out.println("Invalid command! Please type H for list of available commands.");
                    } catch (EmptyCommandException e) {
                        LOGGER.error(e.getMessage(), e);
                        System.out.println("For available commands, please, enter H");
                    } catch (InvalidArgumentException e) {
                        LOGGER.error(e.getMessage(), e);
                        System.out.println("Invalid argument for command " + e.getCommandEnum()
                                + ". " + e.getMessage() + ". Please type H for help.");
                    } catch (NotEnoughParametersException e) {
                        LOGGER.error(e.getMessage(), e);
                        System.out.println("Not enough parameters for command " +
                                e.getCommandEnum().getName() + ". Please type H for help.");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("Fatal error, for more info please see app.log");
            LOGGER.error("Fatal Error", e);
        }
    }
}