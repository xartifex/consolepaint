package com.xartifex.consolepaint;

public class CommonConstants {
    public static final int MAX_WIDTH = 1000;
    public static final int MAX_HEIGHT = 1000;
    public static final int MAX_X = MAX_WIDTH + 50000;
    public static final int MIN_X = -MAX_WIDTH - 50000;
    public static final int MAX_Y = MAX_HEIGHT + 50000;
    public static final int MIN_Y = -MAX_HEIGHT - 50000;
    public static final String NL = System.lineSeparator();

    public static final Object[] EMPTY_OBJ_ARRAY = new Object[0];
}
