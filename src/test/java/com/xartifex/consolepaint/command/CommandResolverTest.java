package com.xartifex.consolepaint.command;

import com.xartifex.consolepaint.exception.EmptyCommandException;
import com.xartifex.consolepaint.exception.InvalidArgumentException;
import com.xartifex.consolepaint.exception.NotEnoughParametersException;
import com.xartifex.consolepaint.exception.UnknownCommandException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommandResolverTest {

    private static CommandResolver commandResolver;

    @BeforeClass
    public static void globalSetup() {
        commandResolver = new CommandResolver();
    }

    @Test(expected = UnknownCommandException.class)
    public void unknownCommand() {
        commandResolver.resolve("Foo 123 2 1");
    }

    @Test(expected = EmptyCommandException.class)
    public void emptyCommand() {
        commandResolver.resolve("     \r \r   \r\n          ");
    }

    @Test(expected = InvalidArgumentException.class)
    public void invalidArgument() {
        commandResolver.resolve("C Bar");
    }

    @Test(expected = NotEnoughParametersException.class)
    public void notEnoughParameters() {
        commandResolver.resolve("C 1");
    }

    @Test
    public void resolveC() {
        Integer[] args = {20, 4};
        Command expectedC = new Command(CommandEnum.C, args);

        assertEquals(expectedC, commandResolver.resolve("C 20 4"));
    }

    @Test
    public void resolveL() {
        Integer[] args = {1, 2, 6, 2};
        Command expectedC = new Command(CommandEnum.L, args);

        assertEquals(expectedC, commandResolver.resolve("L 1 2 6 2"));
    }

    @Test
    public void resolveR() {
        Integer[] args = {14, 1, 18, 3};
        Command expectedC = new Command(CommandEnum.R, args);

        assertEquals(expectedC, commandResolver.resolve("R 14 1 18 3"));
    }

    @Test
    public void resolveB() {
        Object[] args = {10, 3, 'o'};
        Command expectedC = new Command(CommandEnum.B, args);

        assertEquals(expectedC, commandResolver.resolve("  B    10     3           okkkk"));
    }
}