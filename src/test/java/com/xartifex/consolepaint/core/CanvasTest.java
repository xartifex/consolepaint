package com.xartifex.consolepaint.core;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringWriter;

import static com.xartifex.consolepaint.CommonConstants.NL;
import static org.junit.Assert.*;

public class CanvasTest {

    private Canvas canvas;
    private PrintWriter consoleOut;
    private StringWriter stringOut;

    @Before
    public void setUp() {
        canvas = new Canvas(20, 4);
        consoleOut = new PrintWriter(System.out);
        stringOut = new StringWriter();

    }

    @After
    public void afterUp() {
        consoleOut.flush();
        canvas = null;
    }

    static final String EXPECTED_EMPTY_CANVAS =
                    "----------------------" + NL +
                    "|                    |" + NL +
                    "|                    |" + NL +
                    "|                    |" + NL +
                    "|                    |" + NL +
                    "----------------------" + NL;

    @Test
    public void drawEmpty3By3Canvas() {
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_EMPTY_CANVAS, stringOut.toString());
    }

    static final String EXPECTED_HORIZONTAL_LINE =
                    "----------------------" + NL +
                    "|                    |" + NL +
                    "|xxxxxx              |" + NL +
                    "|                    |" + NL +
                    "|                    |" + NL +
                    "----------------------" + NL;

    @Test
    public void drawHorizontalLine() {
        canvas.drawLine(new Point(-100, 2), new Point(6, 2));
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_HORIZONTAL_LINE, stringOut.toString());
    }

    @Test
    public void drawHorizontalLineSwitched() {
        canvas.drawLine(new Point(6, 2), new Point(-100, 2));
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_HORIZONTAL_LINE, stringOut.toString());
    }


    static final String EXPECTED_VERTICAL_LINE =
                    "----------------------" + NL +
                    "|                    |" + NL +
                    "|                    |" + NL +
                    "|     x              |" + NL +
                    "|     x              |" + NL +
                    "----------------------" + NL;

    @Test
    public void drawVerticalLine() {
        canvas.drawLine(new Point(6, 3), new Point(6, 4));
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_VERTICAL_LINE, stringOut.toString());
    }

    @Test
    public void drawVerticalLineSwitched() {
        canvas.drawLine(new Point(6, 4), new Point(6, 3));
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_VERTICAL_LINE, stringOut.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void drawLineFail() {
        canvas.drawLine(new Point(2, -100), new Point(100, 2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void outOfRangeLine() {
        canvas.drawLine(new Point(-100, 2), new Point(-50, 2));
    }

    static final String EXPECTED_RECTANGLE =
                    "----------------------" + NL +
                    "|             xxxxx  |" + NL +
                    "|             x   x  |" + NL +
                    "|             xxxxx  |" + NL +
                    "|                    |" + NL +
                    "----------------------" + NL;
    @Test
    public void drawRectangle() {
        canvas.drawRectangle(new Point(14, 1), new Point(18, 3));
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_RECTANGLE, stringOut.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void outOfRangeRectangle() {
        canvas.drawRectangle(new Point(21, 4), new Point(30, 30));
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidRectanglePoints() {
        canvas.drawRectangle(new Point(21, 4), new Point(15, 15));
    }

    static final String EXPECTED_FILLED_CANVAS =
                    "----------------------" + NL +
                    "|oooooooooooooxxxxxoo|" + NL +
                    "|xxxxxxooooooox   xoo|" + NL +
                    "|     xoooooooxxxxxoo|" + NL +
                    "|     xoooooooooooooo|" + NL +
                    "----------------------" +  NL;
    @Test
    public void fillSomeArea() {
        canvas.drawLine(new Point(-100, 2), new Point(6, 2));
        canvas.drawLine(new Point(6, 3), new Point(6, 4));
        canvas.drawRectangle(new Point(14, 1), new Point(18, 3));
        canvas.fill(new Point(10,3), 'o');
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_FILLED_CANVAS, stringOut.toString());
    }

    static final String EXPECTED_FILL_FOR_BORDER =
                    "----------------------" + NL +
                    "|             kkkkk  |" + NL +
                    "|             k   k  |" + NL +
                    "|             kkkkk  |" + NL +
                    "|                    |" + NL +
                    "----------------------" + NL;
    @Test
    public void fillBorder() {
        canvas.drawRectangle(new Point(14, 1), new Point(18, 3));
        canvas.fill(new Point(14,1), 'k');
        canvas.draw(consoleOut);
        canvas.draw(stringOut);
        assertEquals(EXPECTED_FILL_FOR_BORDER , stringOut.toString());
    }
}