Requirements:
* Java 8
* Maven

From command line:

Build:
mvn package

Run:
java -jar target/console-paint-1.0-SNAPSHOT-jar-with-dependencies.jar

Logs are written to logs/ directory

Assumptions/Limitations:
* MAX_WIDTH and MAX_HEIGHT are equal to 1000, which seems to be enough for most terminals.
* values for x coordinate are limited from -MAX_WIDTH-50000 to MAX_WIDTH+50000 
* values for y coordinate are limited from -MAX_HEIGHT-50000 to MAX_HEIGHT+50000
Such limitation on coordinates doesn't seem to influence functional capabilities of the application.